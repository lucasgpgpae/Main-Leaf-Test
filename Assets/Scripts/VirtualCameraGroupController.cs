﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class VirtualCameraGroupController : MonoBehaviour
{

    public enum Modes
    {
        Third_Person,
        Locked_Camera
    }
    public Modes cameraMode;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void ModeSwitcher()
    {
        switch(cameraMode)
        {
            case Modes.Third_Person:

                SetCamera("ThirdPerson_VCamera");

                break;
            case Modes.Locked_Camera:

                SetCamera("Locked1_VCamera");

                break;
        }
    }

    public void SetCamera(string name)
    {
        foreach(Transform virtualCam in transform)
        {
            if(virtualCam.gameObject.name == name)
            {
                virtualCam.GetComponent<CinemachineVirtualCamera>().Priority = 1;
            }
            else
            {
                virtualCam.GetComponent<CinemachineVirtualCamera>().Priority = 0;
            }
        }
    }
}
