﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BoxPositionData
{
    public bool dataSaved;
    public float posX, posY, posZ;

#region SaveData

    public void SavePos(Vector3 pos)
    {
        posX = pos.x;
        posY = pos.y;
        posZ = pos.z;
        dataSaved = true;
    }

#endregion
}
