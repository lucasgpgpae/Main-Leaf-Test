﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    public bool lockMouse;
    public float velocity;

    private float mouseX, mouseY;

    void Start()
    {
        LockMouse(lockMouse);
    }

    void Update()
    {
        mouseX += Input.GetAxis("Mouse X") * velocity;
        mouseY -= Input.GetAxis("Mouse Y") * velocity;

        transform.eulerAngles = new Vector3(0f, mouseX, 0f);
    }

    public void LockMouse(bool toggle)
    {
        if(toggle)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }

    }
}
