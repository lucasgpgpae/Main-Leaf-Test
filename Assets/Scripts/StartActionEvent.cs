﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StartActionEvent : MonoBehaviour
{
    [System.Serializable] 
    public class StartEventAction : UnityEvent {};
    public StartEventAction startAction;

    void Start()
    {
        startAction.Invoke();
    }
}
