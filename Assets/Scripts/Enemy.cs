﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    NavMeshAgent agent;

    public enum EnemyStates
    {
        WAITING,
        WALKING,
        HUNTING
    }
    public EnemyStates state;

    [Header("Path options")]
    public List<Transform> pathPositions;
    int actualPathId = -1;
    Transform actualPath;

    [Header("Time Options")]
    public float waitingTime = 5f;
    float nextStateChange;

    [Header("Others")]
    GameObject player;
    NavMeshPath navMeshPath;
    Animator anim;


    void Start()
    {
        navMeshPath = new NavMeshPath();
        agent = GetComponent<NavMeshAgent>();
        anim = transform.GetChild(0).GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        nextStateChange = Time.time+waitingTime;
    }

    void Update()
    {
        EnemyState();
    }

    void EnemyState()
    {
        switch(state)
        {
            case EnemyStates.WAITING:
                HuntChecker();
                anim.SetBool("Walking", false);
                if(Time.time >= nextStateChange)
                {
                    actualPath = ChoosePath();
                    state = EnemyStates.WALKING;
                }

                break;

            case EnemyStates.WALKING:

                HuntChecker();
                anim.SetBool("Walking", true);
                if(actualPath != null)
                {
                    agent.SetDestination(actualPath.position);

                    if(Vector3.Distance(transform.position, actualPath.position) <= 1f)
                    {
                        nextStateChange = Time.time+waitingTime;
                        actualPath = null;
                        state = EnemyStates.WAITING;
                    }
                } 
                else state = EnemyStates.WALKING;

                break;

            case EnemyStates.HUNTING:
                if(Vector3.Distance(player.transform.position, transform.position) > 1f)
                {
                    agent.SetDestination(player.transform.position);
                    anim.SetBool("Walking", true);
                }
                else
                {
                    agent.SetDestination(transform.position);
                    anim.SetBool("Walking", false);
                }
                break;
        }
    }

    void HuntChecker()
    {
        if(Vector3.Angle(player.transform.position, transform.forward) < 5.0f)
        {
            CheckHuntPath();
        }
        else if(Vector3.Distance(player.transform.position, transform.position) < 3f)
        {
            CheckHuntPath();
        }
    }

    void CheckHuntPath()
    {
        if(CheckPath(player.transform))
        {
            player.GetComponent<Player>().playerState = Player.PlayerStates.Hunted;
            state = EnemyStates.HUNTING;
        }
    }

    bool CheckPath(Transform target)
    {
        agent.CalculatePath(target.position, navMeshPath);
        if (navMeshPath.status != NavMeshPathStatus.PathComplete) 
        {
            return false;
        }
        else 
        {
            return true;
        }
    }

    Transform ChoosePath()
    {
        if(actualPathId+1 < pathPositions.Count)
        {
            actualPathId++;
        }
        else
        {
            actualPathId = 0;
        }

        return pathPositions[actualPathId];
    }
}
