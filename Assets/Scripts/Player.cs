﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;
using System;

public class Player : MonoBehaviour
{
    public enum PlayerStates
    {
        Idle,
        Walk,
        Jump,
        Crouch,
        Grab,
        Hunted
    }
    public PlayerStates playerState;

    [Header("Configs")]
    public float moveSpeed;
    public float rotationSpeed;
    public float jumpForce;
    public GameObject playerModel;
    public GameData gameData;
    public CapsuleCollider normalCollider;
    public CapsuleCollider crouchCollider;
    public Box nearBox;
    public Image actionCircleImage;
    public TMP_Text actionText;
    public GameObject capturedText;

    public PlayerMovement movementMode;
    public enum PlayerMovement
    {
        Locked,
        CameraBased
    }

    private Vector3 moveDirection = Vector3.zero;
    private Rigidbody rb;
    private Animator anim;
    private bool huntedActions;
    private float mouseX;

    void Start()
    {
        if(gameData.gameStarted && (gameData.actualScene == SceneManager.GetActiveScene().name))
        {
            transform.position = new Vector3(gameData.playerData.posX, gameData.playerData.posY, gameData.playerData.posZ);
            transform.rotation = new Quaternion(gameData.playerData.rotX, gameData.playerData.rotY, gameData.playerData.rotZ, gameData.playerData.rotW);
        }
        else if(!gameData.gameStarted) gameData.gameStarted = true;

        rb = GetComponent<Rigidbody>();
        anim = playerModel.GetComponent<Animator>();
        mouseX = transform.rotation.eulerAngles.y;
    }

    void Update() 
    {
        PlayerStateMachine();
        AnimationState();
        SavePlayerPosition();

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(Time.timeScale == 1)
                gameData.PauseGame();
            else
                gameData.ResumeGame();
        }
    }

    void SavePlayerPosition()
    {
        gameData.actualScene = SceneManager.GetActiveScene().name;
        gameData.playerData.SavePos(transform.position);
        gameData.playerData.SaveRot(transform.rotation);
        gameData.SaveGame();
    }

    void PlayerStateMachine()
    {
        switch(playerState)
        {
            case PlayerStates.Idle:

                MovementController();
                JumpController();
                CrouchController();
                GrabController();

                if(isMoving()) playerState = PlayerStates.Walk;
                if(isJumping()) playerState = PlayerStates.Jump;

                break;
            case PlayerStates.Walk:

                MovementController();
                JumpController();
                GrabController();

                if(!isMoving()) playerState = PlayerStates.Idle;
                if(isJumping()) playerState = PlayerStates.Jump;

                break;
            case PlayerStates.Jump:

                MovementController();
                GrabBlocker();

                if(!isJumping() && !isMoving()) playerState = PlayerStates.Idle;
                else if(!isJumping() && isMoving()) playerState = PlayerStates.Walk;

                break;
            case PlayerStates.Crouch:

                MovementController();
                CrouchController();
                GrabBlocker();

                break;
            case PlayerStates.Grab:

                GrabbedMovementController();
                GrabController();

                break;
            case PlayerStates.Hunted:
                if(!huntedActions)
                {
                    huntedActions = true;
                    HuntedDown();
                }
                break;
        }
    }

    void AnimationState()
    {
        switch(playerState)
        {
            case PlayerStates.Idle:

                anim.SetBool("Walking", false);
                anim.SetBool("Jumping", false);
                anim.SetBool("Crouching", false);

                break;
            case PlayerStates.Walk:

                anim.SetBool("Walking", true);
                anim.SetBool("Jumping", false);
                anim.SetBool("Crouching", false);
                
                break;
            case PlayerStates.Jump:

                anim.SetBool("Walking", false);
                anim.SetBool("Jumping", true);
                anim.SetBool("Crouching", false);

                break;
            case PlayerStates.Crouch:

                anim.SetBool("Walking", isMoving());
                anim.SetBool("Jumping", false);
                anim.SetBool("Crouching", true);

                break;
            case PlayerStates.Grab:

                anim.SetBool("Walking", isMoving());
                anim.SetBool("Jumping", false);
                anim.SetBool("Crouching", true);

                break;
            case PlayerStates.Hunted:

                anim.SetBool("Walking", false);
                anim.SetBool("Jumping", false);
                anim.SetBool("Crouching", false);

                break;
        }
    }

#region CONTROLLERS

    void JumpController()
    {
        if(Input.GetButtonDown("Jump"))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
    }

    void CrouchController()
    {
        if(Input.GetButtonDown("Crouch"))
        {
            if(playerState == PlayerStates.Crouch)
            {
                normalCollider.enabled = true;
                crouchCollider.enabled = false;

                if(isMoving())
                    playerState = PlayerStates.Walk;
                else
                    playerState = PlayerStates.Idle;
            }
            else
            {
                playerState = PlayerStates.Crouch;
                crouchCollider.enabled = true;
                normalCollider.enabled = false;
            }
        }
    }

    void MovementController()
    {
        if(Time.timeScale > 0)
        {
            float desiredMoveSpeed = playerState == PlayerStates.Crouch ? moveSpeed/3 : moveSpeed;

            switch(movementMode)
            {
                case PlayerMovement.Locked:
                    float mHLocked = Input.GetAxis("Horizontal");
                    float mVLocked = Input.GetAxis("Vertical") * -1;
                    moveDirection = new Vector3(mVLocked, rb.velocity.y, mHLocked);

                    rb.velocity = new Vector3(mVLocked* desiredMoveSpeed, rb.velocity.y, mHLocked * desiredMoveSpeed);

                    if(isMoving())
                    {
                        Quaternion toRotation = Quaternion.LookRotation(moveDirection , Vector3.up);
                        toRotation = new Quaternion(0f, toRotation.y, 0f, toRotation.w);
                        rb.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed);
                    }
                    break;

                case PlayerMovement.CameraBased:

                    mouseX += Input.GetAxis("Mouse X") * 2f;
                    transform.eulerAngles = new Vector3(0f, mouseX, 0f);

                    float mHCam = Input.GetAxis("Horizontal");
                    float mVCam = Input.GetAxis("Vertical");
                    rb.velocity = transform.TransformDirection( new Vector3(mHCam * desiredMoveSpeed, rb.velocity.y, mVCam * desiredMoveSpeed) );
                    
                    break;
            }
        }

    }

    void GrabbedMovementController()
    {
        //float mH = Input.GetAxis("Horizontal") * -1;
        float mV = Input.GetAxis("Vertical") * -1;
        moveDirection = new Vector3(0f, rb.velocity.y, mV);
        float desiredMoveSpeed = moveSpeed/3;


        rb.velocity = transform.forward * Input.GetAxis("Vertical") * desiredMoveSpeed;
        Rigidbody boxRb = nearBox.GetComponent<Rigidbody>();

        if(Mathf.Abs(boxRb.velocity.y) >= 2)
        {
            if(isMoving())
                playerState = PlayerStates.Walk;
            else
                playerState = PlayerStates.Idle;
        }

        boxRb.velocity = new Vector3(rb.velocity.x*1.15f, boxRb.velocity.y, rb.velocity.z*1.15f);
    }

    void GrabController()
    {
        NearBox();

        if(nearBox != null)
        {
            if(playerState != PlayerStates.Grab)
            {
                if(Input.GetButtonDown("Grab"))
                {
                    GrabChangePlayerPosition();
                    playerState = PlayerStates.Grab;
                }
            }
            else
            {
                if(Input.GetButtonDown("Grab"))
                {
                    if(isMoving())
                        playerState = PlayerStates.Walk;
                    else
                        playerState = PlayerStates.Idle;
                }
            }
        }
        else
        {
            if(playerState == PlayerStates.Grab)
            {
                if(isMoving())
                    playerState = PlayerStates.Walk;
                else
                    playerState = PlayerStates.Idle;
            }
        }
    }

    void GrabChangePlayerPosition()
    {
        float angle = transform.rotation.eulerAngles.y;
        List<float> angles = new List<float>(){0, 90, 180, 270, 360};

        float resultAngle = angle;
        float nearest = 1000f;

        foreach(float value in angles)
        {
            if(Mathf.Abs(angle - value) <= nearest)
            {
                resultAngle = value;
                nearest = Mathf.Abs(angle - value);
            }
        }

        transform.rotation = Quaternion.Euler(0f, resultAngle, 0f);
    }

    void GrabBlocker()
    {
        nearBox = null;
        actionCircleImage.color = Color.grey;
        actionText.text = "...";
    }

#endregion

#region VERIFICATIONS

    void NearBox()
    {
        List<Box> boxes = new List<Box>(FindObjectsOfType<Box>());
        Box foundNearBox = null;

        foreach(Box box in boxes)
        {
            if(Vector3.Distance(transform.position, box.transform.position) <= 1.5f && (box.transform.position.y > transform.position.y))
            {
                if(Vector3.Dot(transform.forward, (box.transform.position - transform.position).normalized) >= 0.7f)
                {
                    foundNearBox = box;
                }
            }
        }

        nearBox = foundNearBox;

        if(nearBox != null)
        {
            actionCircleImage.color = Color.blue;
            actionText.text = "GRAB BOX";
        }
        else
        {
            actionCircleImage.color = Color.grey;
            actionText.text = "...";
        }
    }

    bool isMoving()
    {
        if(rb.velocity.x != 0 || rb.velocity.z != 0)
        {
            return true;
        }
        return false;
    }

    bool isJumping()
    {
        bool isCollidingWithGround = Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), 0.1f, 1 << 8);
        anim.SetBool("Jumping", true);

        /*
        Color rayColor;
        if(isCollidingWithGround) rayColor = Color.green; else rayColor = Color.red;
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * 0.1f, rayColor);
        */

        return !isCollidingWithGround;
    }

    private void OnCollisionEnter(Collision col) 
    {
        if(col.gameObject.tag == "Reset")
        {
            gameData.Respawn();
        }

        if(col.gameObject.tag == "PassPhase")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }
    }

    private void OnTriggerEnter(Collider col) 
    {
        GameObject triggerObject = col.gameObject;
        switch(triggerObject.tag)
        {
            case "ChangeCam":

                FindObjectOfType<VirtualCameraGroupController>().SetCamera(triggerObject.transform.parent.name);
                
                break;

            case "Coin":

                triggerObject.transform.parent.parent.GetComponent<Coin>().Collect();

                break;
        }
    }

#endregion

    async void HuntedDown()
    {
        capturedText.SetActive(true);
        await Task.Delay(TimeSpan.FromSeconds(5f));
        gameData.HuntedByEnemy();
    }

}
