﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    public int id;
    public GameData gameData;
    
    void Start() 
    {
        if(gameData.boxesPosition[id-1].dataSaved)
        {
            BoxPositionData data = gameData.boxesPosition[id-1];
            transform.position = new Vector3(data.posX, data.posY, data.posZ);
        }
    }

    void Update()
    {
        SaveBoxPosition();
    }

    void SaveBoxPosition()
    {
        gameData.boxesPosition[id-1].SavePos(transform.position);
    }
}
