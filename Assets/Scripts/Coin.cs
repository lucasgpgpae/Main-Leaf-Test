﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public int id;
    public GameData gameData;

    void Start() 
    {
        if(gameData.coinsCollected[id-1])
            DisableCoin();
    }

    public void DisableCoin()
    {
        gameObject.SetActive(false);
    }

    public void EnableCoin()
    {
        gameObject.SetActive(true);
    }

    public void Collect()
    {
        if(!gameData.coinsCollected[id-1])
        {
            gameData.coinsCollected[id-1] = true;
            DisableCoin();
        }
    }
}
