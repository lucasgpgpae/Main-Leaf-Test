﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonBases : MonoBehaviour
{
    public GameData gameData;

    public void StartNewGame()
    {
        gameData.actualScene = "";
        gameData.playerData = new PlayerPositionData();
        gameData.boxesPosition = new List<BoxPositionData>(){new BoxPositionData(), new BoxPositionData()};
        gameData.coinsCollected = new List<bool>(){false, false, false, false, false, false};
        gameData.gameStarted = false;
        SceneManager.LoadScene("Game_Boxes");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ChangeScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void DestroyGameObject(GameObject objectToDestroy)
    {
        Destroy(objectToDestroy);
    }

    public void InstantiatePopup(string name)
    {
        Instantiate(Resources.Load("Popups/"+name));
    }

}
