﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CoinsText : MonoBehaviour
{
    public GameData gameData;
    TMP_Text coinTxt;
    void Start()
    {
        coinTxt = GetComponent<TMP_Text>();
    }

    void Update()
    {
        if((gameData != null) && (coinTxt != null))
        {
            CountCoins();
        }
    }

    void CountCoins()
    {
        int collectedCoins = 0;
        foreach(bool collected in gameData.coinsCollected)
        {
            if(collected) collectedCoins++;
        }
        coinTxt.text = "COINS: "+collectedCoins;
    }
}
