﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;

public class ContinueButton : MonoBehaviour
{
    public GameData gameData;

    void Update() 
    {
        if(PlayerPrefs.GetString("SavedData", "") != "")
            GetComponent<Button>().interactable = true;
        else
            GetComponent<Button>().interactable = false;
    }

    public void ContinueGame()
    {
        if(PlayerPrefs.GetString("SavedData", "") != "")
        {
            gameData = JsonConvert.DeserializeObject<GameData>(PlayerPrefs.GetString("SavedData"));
            SceneManager.LoadScene(gameData.actualScene);
        }
        else
        {
            Debug.LogError("Saved Data Not Found!");
        }
    }
}
