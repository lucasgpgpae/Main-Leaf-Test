using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;

[CreateAssetMenu(fileName = "GameData", menuName = "ScriptableObjects/GameDataObject", order = 1)]
public class GameData : ScriptableObject
{
    public bool gameStarted;
    public string actualScene;
    [SerializeField] public PlayerPositionData playerData;
    [SerializeField] public List<BoxPositionData> boxesPosition;
    public List<bool> coinsCollected;

    public void Respawn()
    {
        gameStarted = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1;
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
        Time.timeScale = 1;
        SaveGame();
    }

    public void HuntedByEnemy()
    {
        SceneManager.LoadScene("Game_Boxes");
    }

    public void PauseGame()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        GameObject pauseMenu = Instantiate(Resources.Load("Popups/PauseMenu") as GameObject);
        pauseMenu.name = "PauseMenu";
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Destroy(GameObject.Find("PauseMenu"));
        Time.timeScale = 1;
    }

    public void SaveGame()
    {
        if(gameStarted)
        {
            string data = JsonConvert.SerializeObject(this);
            PlayerPrefs.SetString("SavedData", data);
        }
    }

    public void DeleteSave()
    {
        actualScene = "";
        playerData = new PlayerPositionData();
        boxesPosition = new List<BoxPositionData>(){new BoxPositionData(), new BoxPositionData()};
        coinsCollected = new List<bool>(){false, false, false, false, false, false};
        gameStarted = false;
        PlayerPrefs.SetString("SavedData", "");
    }
}
